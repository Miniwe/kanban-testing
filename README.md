# *Kanban Selenium Testing* #

Руководство начальное http://selenium2.ru/docs/selenium-ide.html

Установить в Firefox:

* https://addons.mozilla.org/ru/firefox/addon/selenium-ide/
* [опционально] https://addons.mozilla.org/en-US/firefox/addon/selenium-expert-selenium-ide/
* [опционально] https://addons.mozilla.org/ru/firefox/addon/flow-control/
* [опционально] https://github.com/agileadam/selenium-lipsumtext


**Условия**

* Пользователь должен быть авторизован в Kanban и иметь Google Drive
* Для успешного прохождения _всех_ тестов лучше пользоваться русской версией Google Drive (нажатия кнопок не получается реализовать для любых языков)
* Все борды лучше хранить в корневой папке
* Если тест не проходит то можно проверить что нет открытых борд изначально - проверки что борд открыто 4 пока нет
* Для проверки Assign необходимо иметь +2 пользователя с доступом к Testing Card Dashboard
* Пока тесты не обкатаны бывают ошибки связанные с некоторыми неточностями - после прохождени набора тестов отдельные монжо бывает перезапустить с удачным результатом


Для запуска скриптов надо:

1. запустить Selenium Ide из меню Tools
1. открыть нужный Test Suite:

* dashboard.html
* - 12, 14 не завершены - отложено
* card.html
* - завершены: в основном
* list.html
* - завершены


**Todo**

* make all tests create dashboards with unique id (no SelA)
* make export to js and run with phanomjs
* tool: https://www.npmjs.com/package/selenium-html-js-converter
* ...


**Errors**

* удалил борду - восстановил - в диалоге открытия пытаюсь найти доску по названию - ничего не найдено - но(!) доска есть в списке - визуально наблюдается - открыл закрыл диалог открытия/создания
* не получается удалить себя из assigned пользователей для карты